package com.example.student.performance;

public class to_do {

    public String title;
    public String dateadded;
    public String datedue;
    public String category;

    public to_do(String title, String dateadded, String datedue, String category){
        this.title=title;
        this.dateadded = dateadded;
        this.datedue = datedue;
        this.category = category;
    }
}
